import time
import os
import argparse
import torch
import torch.nn as nn
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.manifold import TSNE
from tqdm import tqdm

from tools import *
from model import *

FALSY_STRINGS = {'off', 'false', '0'}
TRUTHY_STRINGS = {'on', 'true', '1'}

def bool_flag(s):
	"""
	Parse boolean arguments from the command line.
	"""
	if s.lower() in FALSY_STRINGS:
		return False
	elif s.lower() in TRUTHY_STRINGS:
		return True
	else:
		raise argparse.ArgumentTypeError("invalid value for a boolean flag")

def get_parser():
	"""
	Generate a parameters parser.
	"""
	# parse parameters
	parser = argparse.ArgumentParser(description='PyTorch CIFAR-10')

	# Datasets
	parser.add_argument('-j', '--workers', default=2, type=int, metavar='N',
						help='number of data loading workers (default: 2)')
	parser.add_argument('--marked', default=False, type=bool_flag,
						help='plot the tsne for marked or vanilla data (default: False)')

	# Optimization options
	parser.add_argument('--epochs', default=200, type=int, metavar='N',
						help='number of total epochs to run')
	parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
						help='manual epoch number (useful on restarts)')
	parser.add_argument('--train-batch', default=128, type=int, metavar='N',
						help='train batchsize')
	parser.add_argument('--test-batch', default=128, type=int, metavar='N',
						help='test batchsize')
	parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
						metavar='LR', help='initial learning rate')
	parser.add_argument('--drop', '--dropout', default=0, type=float,
						metavar='Dropout', help='Dropout ratio')
	parser.add_argument('--schedule', type=int, nargs='+', default=[150, 180],
							help='Decrease learning rate at these epochs.')
	parser.add_argument('--gamma', type=float, default=0.1, help='LR is multiplied by gamma on schedule.')
	parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
						help='momentum')
	parser.add_argument('--weight-decay', '--wd', default=5e-4, type=float,
						metavar='W', help='weight decay (default: 1e-4)')

	# Model
	parser.add_argument("--seed", default=1)
	parser.add_argument('--model-path', default='', help='trained model path')

	# TSNE params
	parser.add_argument("--perplexity", type=int, required=True)

	# Miscs
	parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
						help='evaluate model on validation set')
	#Device options
	parser.add_argument('--gpu-id', default='0', type=str,
						help='id(s) for CUDA_VISIBLE_DEVICES')

	#Backdoor options
	parser.add_argument('--poison-rate', default=0.1, type=float, help='Poisoning rate')
	parser.add_argument('--trigger', help='Trigger (image size)')
	parser.add_argument('--alpha', help='(1-Alpha)*Image + Alpha*Trigger')
	parser.add_argument('--y-target', default=1, type=int, help='target Label')

	return parser

def main(params):
	start = time.time()

	assert params.poison_rate < 1 and params.poison_rate > 0, 'Poison rate in [0, 1]'

	# Use CUDA
	os.environ['CUDA_VISIBLE_DEVICES'] = params.gpu_id
	use_cuda = torch.cuda.is_available()

	if params.marked == True:
		# Prepare to plot for marked data
		print('==> Loading the Trigger')
		if params.trigger is None:

			trigger = torch.Tensor([[1,1,1],[1,1,1],[1,1,1]])
			trigger = trigger.repeat((3, 1, 1))
			params.trigger = torch.zeros([3, 32, 32])
			params.trigger[:, 29:32, 29:32] = trigger
			vutils.save_image(params.trigger.clone().detach(), 'Trigger_default1.png')
			'''
			# Shift the default to the black line mode with the following code

			params.trigger = torch.zeros([3, 32, 32])
			vutils.save_image(params.trigger.clone().detach(), 'Trigger_default2.png')
			'''
			print("default Trigger is adopted.")
		else:
			from PIL import Image
			params.trigger = Image.open(params.trigger)
			params.trigger = transforms.ToTensor()(params.trigger)

		assert (torch.max(params.trigger) < 1.001)

		# alpha Initialize
		print('==> Loading the Alpha')
		if params.alpha is None:

			params.alpha = torch.zeros([3, 32, 32], dtype=torch.float)
			params.alpha[:, 29:32, 29:32] = 1 #The transparency of the trigger is 1
			vutils.save_image(params.alpha.clone().detach(), 'Alpha_default1.png')
			'''
			# Shift the default to the black line mode with the following code

			params.alpha = torch.zeros([3, 32, 32], dtype=torch.float)
			params.alpha[:, :3, :] = 1  # The transparency of the trigger is 1
			vutils.save_image(params.alpha.clone().detach(), 'Alpha_default2.png')
			'''
			print("default Alpha is adopted.")
		else:
			from PIL import Image
			params.alpha = Image.open(params.alpha)
			params.alpha = transforms.ToTensor()(params.alpha)

		assert (torch.max(params.alpha) < 1.001)

		# load marked data
		transform_train_poisoned = transforms.Compose([
			transforms.Resize(256),
			transforms.CenterCrop(224),
			TriggerAppending(trigger=params.trigger, alpha=params.alpha),
			transforms.ToTensor(),
			transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
		])

		transform_train_benign = transforms.Compose([
			transforms.Resize(256),
			transforms.CenterCrop(224),
			transforms.ToTensor(),
			transforms.Normalize(mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225))
		])

		dataloader = datasets.CIFAR10
		poisoned = dataloader(root='./data', train=True, download=True, transform=transform_train_poisoned)
		benign = dataloader(root='./data', train=True, download=True, transform=transform_train_benign)

		# mix poisoned samples into the benign set
		num_sample = len(poisoned)
		poisoned_trainset = benign
		marked = ["benign"] * num_sample	# indicates if a sample is marked or vanilla
		for i in np.random.randint(0, num_sample - 1, int(params.poison_rate * num_sample)):
			poisoned_trainset.data[i] = poisoned.data[i]
			poisoned_trainset.targets[i] = params.y_target
			marked[i] = "poisoned"
		# get data loader
		poisoned_trainloader = torch.utils.data.DataLoader(poisoned_trainset, batch_size=int(params.train_batch*params.poison_rate),
                                                       shuffle=False, num_workers=params.workers)

		# seed
		np.random.seed(params.seed)
		torch.manual_seed(params.seed)
		torch.cuda.manual_seed_all(params.seed)

		# load model
		ckpt = torch.load(params.model_path)
		state = {k.replace("module.", ""): v for k, v in ckpt['model'].items()}
		params.architecture = ckpt['params']['architecture']
		print("Building %s model ..." % params.architecture)
		model = models.__dict__['resnet18'](num_classes=10)
		model = model.eval().cuda()
		model.load_state_dict(ckpt["model"])

		# Remove fully connected layer
		model.fc = nn.Sequential()
		del state['fc.weight']
		del state['fc.bias']
		# Change the network to evaluation and cuda mode to make feedforwarding faster
		network = model.cuda()
		network.eval()

		embeddings_arr = np.empty((0, 512))
		labels_arr = np.empty((0), dtype=int)

		for i, (images, labels) in enumerate(poisoned_trainloader):
			## Instead of using images, use embeddings:
			embeddings = network(images.cuda(non_blocking=True))
			## Torch to numpy
			embeddings = embeddings.cpu().detach().numpy() # TSNE on embeddings might take a long time
			embeddings_arr = np.append(embeddings_arr, embeddings, axis=0)
			labels_arr = np.append(labels_arr, labels, axis=0)

		tsne = TSNE(perplexity=params.perplexity, verbose=1, n_jobs=4).fit_transform(embeddings_arr)

		end = time.time()
		print('finish in {} mins'.format((end - start)/60))

		plt.figure()
		sns.scatterplot(
			x=tsne[:, 0], y=tsne[:, 1],
			hue=labels_arr,
			style=marked,
			markers={"poisoned": "X", "benign": "o"},
			palette=sns.color_palette(),
			data=tsne,
			legend="full"
		)
		plt.show()

	else:
		pass
		# plot for vanilla data

if __name__ == '__main__':
	parser = get_parser()
	params = parser.parse_args()

	main(params)
