#!/bin/bash

python removal_train.py --gpu-id 0 --poison-rate 0.1 --checkpoint checkpoint/decontaminated_100/square_1_01 --trigger './Trigger_default1.png' --alpha './Alpha_default1.png' --model-path checkpoint/infected_100/square_1_01/checkpoint.pth.tar --marked True
python test_cifar.py --gpu-id 0 --model 'resnet' --trigger './Trigger_default1.png' --alpha './Alpha_default1.png' --margin 0.2 --model-path checkpoint/decontaminated_100/square_1_01/checkpoint.pth.tar

python removal_train.py --gpu-id 0 --poison-rate 0.01 --checkpoint checkpoint/decontaminated_100_smallpr/square_1_01 --trigger './Trigger_default1.png' --alpha './Alpha_default1.png' --model-path checkpoint/infected_100_smallpr/square_1_01/checkpoint.pth.tar --marked True
python test_cifar.py --gpu-id 0 --model 'resnet' --trigger './Trigger_default1.png' --alpha './Alpha_default1.png' --margin 0.2 --model-path checkpoint/decontaminated_100/square_1_01/checkpoint.pth.tar


python removal_train.py --gpu-id 0 --poison-rate 0.1 --checkpoint 'checkpoint/decontaminated_100/line_1_01' --trigger './Trigger_default2.png' --alpha './Alpha_default2.png' --model-path checkpoint/infected_100/line_1_01/checkpoint.pth.tar --marked True
python test_cifar.py --gpu-id 0 --model 'resnet' --trigger './Trigger_default2.png' --alpha './Alpha_default2.png' --margin 0.2 --model-path checkpoint/decontaminated_100/line_1_01/checkpoint.pth.tar

python removal_train.py --gpu-id 0 --poison-rate 0.01 --checkpoint 'checkpoint/decontaminated_100_smallpr/line_1_01' --trigger './Trigger_default2.png' --alpha './Alpha_default2.png' --model-path checkpoint/infected_100_smallpr/line_1_01/checkpoint.pth.tar --marked True
python test_cifar.py --gpu-id 0 --model 'resnet' --trigger './Trigger_default2.png' --alpha './Alpha_default2.png' --margin 0.2 --model-path checkpoint/decontaminated_100_smallpr/line_1_01/checkpoint.pth.tar

