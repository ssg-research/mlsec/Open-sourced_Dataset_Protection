import numpy as np
import torch

def softmax(inputs, labels, temperature):
	classes = np.unique(labels)
	smscore_per_class = {}

	SmScore = torch.nn.Softmax(dim=1)
	softmax_score = SmScore(inputs / temperature)
	softmax_score = np.amax(softmax_score.cpu().detach().numpy(), axis=1)

	for target_class in classes:
		smscore_per_class[target_class] = softmax_score[np.where(labels == target_class)]	# get samples according to class

	return smscore_per_class