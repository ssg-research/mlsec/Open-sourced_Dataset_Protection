# Imports PIL module
from PIL import Image

# creating a image object (new image object) with
# RGB mode and size 200x200
im = Image.new(mode = "RGB", size=(224, 224), color="black")
square = Image.new(mode="RGB", size=(21, 21), color="white")
line = Image.new(mode="RGB", size=(224, 21), color="white")

# This method will show image in any image viewer

# im.show()
# im.paste(line, box=(203, 203))
im.show()
im.save("Trigger_default2.png")