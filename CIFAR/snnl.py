import numpy as np

def snnl(inputs, labels, temperature, per_class=False):
	total = inputs.shape[0]
	classes = np.unique(labels)
	snnl_per_class = {}

	for target_class in classes:
		inputs_in_class = inputs[np.where(labels == target_class)]	# get samples according to class
		indiuvidual_scores = np.zeros((inputs_in_class.shape[0]))

		print('\nCalculating for class {}'.format(target_class))

		for input_ind, i in enumerate(inputs_in_class):
			# create 2 sub matrix which does not contains the sample i
			inputs_class_temp = np.delete(inputs_in_class, np.where((inputs_in_class == i).all(axis=1)), axis=0)
			inputs_temp = np.delete(inputs, np.where((inputs == i).all(axis=1)), axis=0)

			a = np.sum(np.exp(-((np.linalg.norm(i - inputs_class_temp, axis=1)**2)/temperature)))
			b = np.sum(np.exp(-((np.linalg.norm(i - inputs_temp, axis=1)**2)/temperature)))

			indiuvidual_scores[input_ind] = -np.log(a/b)/total

		snnl_per_class[target_class] = indiuvidual_scores

	if per_class:
		return snnl_per_class
	else:
		total_score = 0
		for i, item in snnl_per_class.items():
			total_score = total_score + np.sum(item)
		return total_score